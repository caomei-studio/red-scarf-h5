import fabric from 'fabric';

/* eslint-disable */

const isVML = function() { return typeof G_vmlCanvasManager !== 'undefined'; };
fabric.util.object.extend(fabric.Object.prototype, {
    hasRotatingPoint: true,
    cornerSize: 10,
    _drawControl: function(control, ctx, methodName, left, top) {
        if (!this.isControlVisible(control)) {
            return;
        }
        var size = this.cornerSize;
        isVML() || this.transparentCorners || ctx.clearRect(left, top, size, size);

        if (control !== 'tl') ctx['fillRect'](left, top, size, size);
        var SelectedIconImage = new Image();
        if (control === 'tl') {
            SelectedIconImage.src = 'http://img.souche.com/f2e/9373592b8cf051908fd69b271099cd43.png';
            ctx.drawImage(SelectedIconImage, left, top, size, size);
        }
    }
});
const cursorOffset = {
    mt: 0, // n
    tr: 1, // ne
    mr: 2, // e
    br: 3, // se
    mb: 4, // s
    bl: 5, // sw
    ml: 6 // w
};
let degreesToRadians = fabric.util.degreesToRadians;
fabric.util.object.extend(fabric.Canvas.prototype, {
    setCursor: function (value) {
        this.upperCanvasEl.style.cursor = value;
    },
    _getActionFromCorner: function(target, corner) {
        var action = 'drag';
        if (corner) {
            action = (corner === 'ml' || corner === 'mr') ?
            'scaleX' : (corner === 'mt' || corner === 'mb') ?
            'scaleY' : (corner === 'mtr' || corner === 'tl') ? 'rotate' : 'scale';
        }
        return action;
    },
    _setCornerCursor: function(corner, target) {
        if ((corner === 'mtr' || corner === 'tl') && target.hasRotatingPoint) {
            this.setCursor(this.rotationCursor);
        } else if (corner in cursorOffset) {
            this.setCursor(this._getRotatedCornerCursor(corner, target));
        } else {
            this.setCursor(this.defaultCursor);
            return false;
        }
    }
});

/* eslint-enable */
