# h5-fabric-boilerplate

> A Vue.js project

## 介绍

* 根据VUX创建
* 加入eslint自定义规范
* 引入fabric.js
* 加入fabric修改控制角图片和方法的Demo

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
